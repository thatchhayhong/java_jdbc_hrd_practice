package com.company;

public class Operations {
   private String insertStudent = "INSERT INTO students (name,gender,card_id,generation_id) VALUES (?, ?, ?,?)";
   private String deleteStudent = "DELETE FROM students WHERE student_id = ?";
   private String updateStudent = "UPDATE students SET name = ? WHERE student_id = ?";
   private String selectStudent = "SELECT * FROM students";
   private String insertQuiz = "INSERT INTO quiz (student_id ,title ,subject ,date,score  ) VALUES (?, ?, ?,?,?)";
   private String selectInnerQuizStudent="SELECT * FROM students INNER JOIN quiz ON quiz.student_id = students.student_id";

//    INSERT INTO quiz (student_id ,title ,subject ,date,score)
//    VALUES (3,'Java JDBC','java','04 May 2021','100');
   public String getInsertStudent() {
        return insertStudent;
    }
    public String getUpdateStudent() {
        return updateStudent;
    }
    public String getDeleteStudent() {
        return deleteStudent;
    }
    public String getSelectStudent() {
        return selectStudent;
    }
    public String getInsertQuiz() {
        return insertQuiz;
    }

    public String getSelectInnerQuizStudent() {
        return selectInnerQuizStudent;
    }
}
