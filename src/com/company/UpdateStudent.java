package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class UpdateStudent {
    String name;

    UpdateStudent(){
        Connection dbConnection = null;
        try {
            Operations updateStudent= new Operations();
            ConnectionDB getConnect =new ConnectionDB();
            dbConnection=getConnect.connect();
            if (dbConnection != null){
                PreparedStatement statement = dbConnection.prepareStatement(updateStudent.getUpdateStudent());
                statement.setString(1,"Kao"); //Field Name
                statement.setInt(2,2); //Update by ID
                int rowsInserted = statement.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("A new student has been updated successfully!");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }

    }
}
