package com.company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SelectStudent {
    SelectStudent(){
        Connection dbConnection = null;
        try {
            ConnectionDB getConnect =new ConnectionDB();
            dbConnection=getConnect.connect();
            if (dbConnection != null){
                Operations addStudent= new Operations();
                PreparedStatement statement = dbConnection.prepareStatement(addStudent.getSelectStudent());
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    System.out.print(rs.getInt(1));
                    System.out.print(": ");
                    System.out.println(rs.getString(2));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }
}
