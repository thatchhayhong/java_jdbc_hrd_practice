package com.company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SelectQuiz {
    SelectQuiz(){
        Connection dbConnection = null;
        try {
            ConnectionDB getConnect =new ConnectionDB();
            dbConnection=getConnect.connect();
            if (dbConnection != null){
                Operations addStudent= new Operations();
                PreparedStatement statement = dbConnection.prepareStatement(addStudent.getSelectInnerQuizStudent());
                ResultSet rs = statement.executeQuery();
                int studentID;
                String studentName;
                String subject;
                String score;
                while (rs.next()) {
                    studentID=rs.getInt(1);
                    studentName=rs.getString(2);
                    subject=rs.getString(9);
                    score=rs.getString(11);
                    System.out.print(" ID: "+studentID+ " Name: " +studentName+ " Subject: "+subject +" Score: "+ score );
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }
}
