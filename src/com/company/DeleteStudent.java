package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class DeleteStudent {
    DeleteStudent() {
        Connection dbConnection = null;
        try {
            Operations deleteStudent= new Operations();
            ConnectionDB getConnect =new ConnectionDB();
            dbConnection=getConnect.connect();
            if (dbConnection != null){
                PreparedStatement statement = dbConnection.prepareStatement(deleteStudent.getDeleteStudent());
                statement.setInt(1,1);
                int rowsInserted = statement.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("A new student was deleted successfully!");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }
}
