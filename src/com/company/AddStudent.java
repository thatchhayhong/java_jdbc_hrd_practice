package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class AddStudent {
    AddStudent(){
        Connection dbConnection = null;
        try {
            ConnectionDB getConnect =new ConnectionDB();
            dbConnection=getConnect.connect();
            if (dbConnection != null){
                Operations addStudent= new Operations();
                PreparedStatement statement = dbConnection.prepareStatement(addStudent.getInsertStudent());
                //Add student
                statement.setString(1, "chhayhong");
                statement.setString(2, "m");
                statement.setString(3, "000001");
                statement.setString(4, "9");
                int rowsInserted = statement.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("A new student was added successfully!");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
    }
}
